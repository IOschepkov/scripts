import os
import glob
import sys
import shutil
import subprocess
import plistlib
import json
import re

targets_path = "/Users/ivanoschepkov/Development/Empatika/coffee-automatization-ios/DoubleB/Targets"

# except_schemes = ["TestApp", "TestAppAll", "TestApp1", "TestApp2", "TestAppProxy", "CoffeeAutomation", "IIkoHack", "RubeaconDemo", "Coffeetogo", "DoubleB", "CoffeeHostel", "Pastadeli", "MeatMe", "Chikarabar", "Korjov", "ElephantBoutique", "Voda", "Cat", "ShashlykHouse", "FishRice", "CoffeeSnack", "7Donuts", "DashiniPirozhki", "RedCup", "CameraObscura", "SushiMarket", "Magnolia", "HouseCafe", "RubeaconApp", "TargetSnapshot", "Pushkin", "Sushilar", "Ladolchevita", "OsteriaBianka", "PureTaste", "SushiVesla", "NookCoffee", "LES", "Brasserie", "BodruiDen", "BurgerHouse", "Nasushi", "VolgaCoffee", "PizzaPicci", "LightFamily", "Tukano", "BanzayNazarovo", "BanzayUfa", "Durum", "Lepainquotidien", "Yarad", "Yaposhka", "Cosmotheca", "Mantiest", "PirogeriaGata", "Foody", "CoffeeAcademy", "AntiPizza", "Yanina", "Pomidor", "SushiEdut", "KrispyKreme", "Perepechki", "FoodFusion", "Grilnica", "Buono", "MealStreet", "MaxxiCoffee", "ChaychonaTlt", "MacaronDay", "RollAndWok", "SolnceBar", "Giotto", "BonAppetit", "OmNomNom", "Tsezar", "MasterPizza", "Ecspromto", "imarket", "Sushitake", "Parmesan", "Rolltime", "Mrestorator", "Mivako", "PresidentCafe", "HouseMafia", "BlackPearl", "Moreman", "Noodles", "TashirPizza", "SushinSon", "Papagiovanni", "Tsunami", "BoofetSolovyinaya", "SchastyeZdes", "MrSushkin", "SmileCity", "Shtolle", "Fudji", "BurgerClub", "Dimash", "SushiTime", "Panda", "OrangeExpress", "MrWok", "Naruto", "Lublini", "NevSushi", "MyPizza"]
except_schemes = []

for d in os.listdir(targets_path):
	if os.path.isdir(os.path.join(targets_path, d)) and not (d in except_schemes):
		print d
		f = "%s/%s/%s-info.plist" % (targets_path, d, d)
		new_f = "%s/%s/Info.plist" % (targets_path, d)
		try:
			os.rename(f, new_f)
		except:
			pass

		assets = "%s/%s/%s-Images.xcassets" % (targets_path, d, d)
		new_assets = "%s/%s/Images.xcassets" % (targets_path, d)
		try:
			os.rename(assets, new_assets)
		except:
			pass

		assets = "%s/%s/Images-%s.xcassets" % (targets_path, d, d)
		new_assets = "%s/%s/Images.xcassets" % (targets_path, d)
		try:
			os.rename(assets, new_assets)
		except:
			pass
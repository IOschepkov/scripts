import json
import os
import sys
import plistlib
import glob

root = sys.argv[1] + "/DoubleB/Targets"
out_path = sys.argv[2]

read = True
jsonDict = {}

if len(sys.argv) > 3:
	mode = sys.argv[3]
	if (mode == "--write"):
		read = False
		tmp = open(out_path, 'r')
		jsonDict = json.loads(tmp.read())

if read == True:
	res = []
	for d in os.listdir(root):
		if os.path.isdir(os.path.join(root, d)):
			for f in glob.glob('%s/*.plist' % os.path.join(root, d)):
				pl = plistlib.readPlist(f)
				if 'CFBundleIdentifier' in pl:
					app_name = d
					bundle_id = pl['CFBundleIdentifier']
					res.append({"name":d, "bundle_id":bundle_id})

	tmp = open(out_path, 'w')
	resultString = json.dumps({"data": res})
	tmp.write('%s' % (resultString))
else:
	for appDict in jsonDict["data"]:
		app_name = appDict["name"]
		bundle_id = appDict["bundle_id"]

		# Open xcode app plist
		plist = "%s/%s/%s-Info.plist" % (root, app_name, app_name)
		pl = plistlib.readPlist(plist)

		pl['CFBundleIdentifier'] = bundle_id
		plistlib.writePlist(pl, plist)

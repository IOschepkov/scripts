import os
import glob
import sys
import shutil
import subprocess
import plistlib
import json
import re

targets_path = "/Users/ivanoschepkov/Development/Empatika/coffee-automatization-ios/DoubleB/Targets"
new_plist = "/Users/ivanoschepkov/Desktop/AppDeployer/AppDeployer/Scripts/newApp/templateApp/templateApp-info.plist"

for d in os.listdir(targets_path):
	if os.path.isdir(os.path.join(targets_path, d)) and d != 'TestApp':
		f = "%s/%s/%s-info.plist" % (targets_path, d, d)
		infoPlist = "%s/%s/en.lproj/InfoPlist.strings" % (targets_path, d)

		ip = open(infoPlist,"r")
		string = ip.read().decode('utf-8')

		m = re.search('\"CFBundleDisplayName\" = \"(.+)\";', string)
		bundle_name = m.group(1)

		print "%s %s" % (d, bundle_name)

		pl = plistlib.readPlist(f)
		#print type(pl['CFBundleDisplayName'])
		#print type(bundle_name)
		pl['CFBundleDisplayName'] = bundle_name
		plistlib.writePlist(pl, f)


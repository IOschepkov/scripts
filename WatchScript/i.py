import os, sys
from PIL import Image

size_classes = [
    [(48, 48), 'notification38@2x.png'],
    [(55, 55), 'notification42@2x.png'],
    [(58, 58), 'companion_settings@2x.png'],
    [(87, 87), 'companion_settings@3x.png'],
    [(80, 80), 'hom38@2x.png'],
    [(88, 88), 'home42@2x.png'],
    [(172, 172), 'short38@2x.png'],
    [(196, 196), 'short42@2x.png']
]

for infile in sys.argv[1:]:
    for size_class in size_classes:
        # outfile = os.path.splitext(infile)[0] + size_class[1]
        outfile = size_class[1]
        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail(size_class[0], Image.ANTIALIAS)
                im.save(outfile, "PNG")
            except IOError:
                print "cannot create icon for '%s'" % infile


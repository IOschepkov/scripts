require 'Spaceship'

apps = ["com.rubeacon.katana", "com.rubeacon.nogbon", "com.rubeacon.pizzarollo", "com.rubeacon.pizzapaolo", "com.rubeacon.sushibon", "com.rubeacon.sushinori", "com.rubeacon.varezjka"]

Spaceship.login("apple@ru-beacon.ru", "RuBeacon00Apple")
cert = Spaceship.certificate.production.all.first

apps.each do |app_bundle_id|
	puts app_bundle_id
	app = Spaceship::Portal.app.find(app_bundle_id)

	# Create a new certificate signing request
	csr, pkey = Spaceship.certificate.create_certificate_signing_request

	# Use the signing request to create a new push certificate
	push_cert = Spaceship.certificate.production_push.create!(csr: csr, bundle_id: app_bundle_id)

	# Download and save cert to Desktop Push certificate
	x509_certificate = push_cert.download
	p12_cert_path = "%s/%s_push_cert.p12" % ["/Users/ivanoschepkov/Desktop/push_certs", app.name]
	p12 = OpenSSL::PKCS12.create('', 'production', pkey, x509_certificate)
	File.write(p12_cert_path, p12.to_der)

end
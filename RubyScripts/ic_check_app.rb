require 'Spaceship'
require 'json'

app_bundle_id = ARGV[0]

apple_id = ARGV[1]
pass = ARGV[2]

Spaceship::Tunes.login(apple_id, pass)

app = Spaceship::Tunes::Application.find(app_bundle_id)

out_path = File.expand_path('~Desktop/AppDeployerTmp.txt')
print out_path

if app.nil?
	tempHash = {"existed" => false}
	File.open(out_path,"w") do |f|
  		f.write(tempHash.to_json)
	end
else
	tempHash = {"existed" => true}
	File.open(out_path,"w") do |f|
  		f.write(tempHash.to_json)
	end
end
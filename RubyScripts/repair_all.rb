require 'Spaceship'

Spaceship.login("apple@ru-beacon.ru", "RuBeacon00Apple")
# Spaceship.login("bayram@empatika.com", "Empatikaopen123")

# profiles = Spaceship.provisioning_profile.ad_hoc.all
profiles = Spaceship.provisioning_profile.app_store.all

apps = ["com.rubeacon.katana", "com.rubeacon.nogbon", "com.rubeacon.pizzarollo", "com.rubeacon.pizzapaolo", "com.rubeacon.sushibon", "com.rubeacon.sushinori", "com.rubeacon.varezjka"]

profiles.each do |profile|
	if apps.include? profile.app.bundle_id
		puts profile.name
		profile.repair! 
	end
end
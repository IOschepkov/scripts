require 'Spaceship'

app_name = ARGV[0]
app_bundle_id = ARGV[1]

Spaceship::Tunes.login("apple@ru-beacon.ru", "RuBeacon00Apple")

app = Spaceship::Tunes::Application.find(app_bundle_id)

if app.nil?
  app = Spaceship::Tunes::Application.create!(name: app_name, 
                                  primary_language: "Russian", 
                                           version: "1.0",
                                               sku: app_bundle_id, 
                                         bundle_id: app_bundle_id)
  app = Spaceship::Tunes::Application.find(app_bundle_id)
end

v = app.edit_version
details = app.details

details.primary_category = "Apps.Food_Drink"
details.secondary_category = "Lifestyle"

app.update_price_tier!("0")

v.update_rating({
	'CARTOON_FANTASY_VIOLENCE' => 0,
  'REALISTIC_VIOLENCE' => 0,
  'PROLONGED_GRAPHIC_SADISTIC_REALISTIC_VIOLENCE' => 0,
  'PROFANITY_CRUDE_HUMOR' => 0,
  'MATURE_SUGGESTIVE' => 0,
  'HORROR' => 0,
  'MEDICAL_TREATMENT_INFO' => 0,
  'ALCOHOL_TOBACCO_DRUGS' => 0,
  'GAMBLING' => 0,
  'SEXUAL_CONTENT_NUDITY' => 0,
  'GRAPHIC_SEXUAL_CONTENT_NUDITY' => 0,
  
  'UNRESTRICTED_WEB_ACCESS' => 0,
  'GAMBLING_CONTESTS' => 0
})

v.copyright = "2014-2016 ООО \"РуБикон\""
v.support_url['ru'] = "http://ru-beacon.ru/"

v.review_first_name = "Иван"
v.review_last_name = "Ощепков"
v.review_phone_number = "+79152975079"
v.review_email = "isoschepkov@gmail.com"
v.review_notes = "Please, use this test card:\n\n4111 1111 1111 1111\n12/19\nname surname\n123"

v.release_on_approval = false

v.save!
details.save!
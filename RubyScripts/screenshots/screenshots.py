from PIL import Image
import json
import sys
import os
import glob

folder_path = sys.argv[1]
inter_path = sys.argv[2]

res = [[], [], [], []]

for f in glob.glob('%s/*.jpg' % folder_path):
    with Image.open(f) as im:
        width, height = im.size

        if height == 960:
            res[0].append(f)
        if height == 1136:
            res[1].append(f)
        if height == 1334:
            res[2].append(f)
        if height == 2208:
            res[3].append(f)

tmp = open(inter_path, 'w')
resultString = json.dumps(res)
tmp.write('%s' % (resultString))
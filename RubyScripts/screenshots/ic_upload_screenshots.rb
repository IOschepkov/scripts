require 'Spaceship'
require 'json'

app_bundle_id = ARGV[0]
folder_path = ARGV[1]

apple_id = ARGV[2]
pass = ARGV[3]

out_path = ARGV[4]

# out_path = "/Users/ivanoschepkov/Desktop/AppDeployerTmp.txt"
system("python screenshots.py " + folder_path + " " + out_path)

file = File.open(out_path, "r")
contents = file.read
data_hash = JSON.parse(contents)


Spaceship::Tunes.login(apple_id, pass)
app = Spaceship::Tunes::Application.find(app_bundle_id)
v = app.edit_version

devices = ["iphone35", "iphone4", "iphone6", "iphone6Plus"]

# Remove screenshots
puts "Removing old screenshots..."
puts ""
for k in 0..3
	for i in 1..5
		begin
			v.upload_screenshot!(nil, i, "ru", devices[k])
		rescue
		end
	end
end

# Upload screenshots
for k in 0..(data_hash.count-1)
	device_screens = data_hash[k]
	for i in 0..(device_screens.count-1)
		if i >= 0 && i < 5
			screenshot = device_screens[i]
			puts "Uploading #{screenshot}..."
			v.upload_screenshot!(screenshot, i+1, "ru", devices[k])
		end
	end
end 

v.save!
import os
import subprocess

workspace_path = '/Users/ivanoschepkov/Development/Other/TemplateApps/TemplateApps.xcworkspace'
scheme_path = 'HouseCafe'
output_directory = '/Users/ivanoschepkov/Desktop/AppDeployerTemp/builds'
output_name = scheme_path
configuration = 'Release'

temp_path = '/Users/ivanoschepkov/Desktop/AppDeployerTemp/app_build_temp.txt'

cmd = 'gym -w %s -s %s -o %s -n %s -q %s' % (workspace_path, scheme_path, output_directory, output_name, configuration)
f = open(temp_path, "w")

subprocess.call(cmd.split(), stdout=f)
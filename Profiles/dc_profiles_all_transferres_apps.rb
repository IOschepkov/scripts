require 'pathname'
require 'Spaceship'

Spaceship.login("bayram@empatika.com", "Empatikaopen123")
cert = Spaceship.certificate.production.all.first


# file = File.read('temp.txt')
# target_apps = file.split(',')
target_apps = ["Sushilar", "AntiPizza"]

all_apps = Spaceship.app.all
all_apps.each do |app|
	app_name = app.name
	app_name.slice! "Empatika"
	app_bundle_id = app.bundle_id

	if target_apps.include? app_name
		puts app_name

		# Enable Push Notifications
		app.update_service(Spaceship.app_service.push_notification.on)

		# Create a new certificate signing request
		csr, pkey = Spaceship.certificate.create_certificate_signing_request

		# Use the signing request to create a new push certificate
		push_cert = Spaceship.certificate.production_push.create!(csr: csr, bundle_id: app_bundle_id)

		# Download and save cert to Desktop Push certificate
		x509_certificate = push_cert.download
		p12_cert_path = "/Users/ivanoschepkov/Desktop/PushCertificates/%s_push_cert.p12" % [app_name]
		p12 = OpenSSL::PKCS12.create('', 'production', pkey, x509_certificate)
		File.write(p12_cert_path, p12.to_der)


		# AdHoc Profiles will add all devices by default
		profile = Spaceship.provisioning_profile.ad_hoc.create!(bundle_id: app_bundle_id, certificate: cert, name: "Emp%s-AdHoc" % [app_name])

		# AdHoc Profiles will add all devices by default
		profile = Spaceship.provisioning_profile.app_store.create!(bundle_id: app_bundle_id, certificate: cert, name: "Emp%s-AppStore" % [app_name])
	end
end

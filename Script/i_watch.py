import os, sys
from PIL import Image

size_classes = [
    [(48, 48), 'watch_ntf_38@2x.png'],
    [(55, 55), 'watch_ntf_42@2x.png'],
    [(58, 58), 'watch_comp@2x.png'],
    [(87, 87), 'watch_comp@3x.png'],
    [(80, 80), 'watch_home@2x.png'],
    [(172, 172), 'watch_look_38@2x.png'],
    [(196, 196), 'watch_look_42@2x.png']
]

icon_path = sys.argv[1]

for size_class in size_classes:
    outfile = size_class[1]
    if icon_path != outfile:
        try:
            im = Image.open(icon_path)
            im.thumbnail(size_class[0], Image.ANTIALIAS)
            im.save(outfile, "PNG")
        except IOError:
            print "cannot create icon for '%s'" % infile


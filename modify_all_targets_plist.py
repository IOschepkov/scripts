import os
import glob
import sys
import shutil
import subprocess
import plistlib
import json
import re

targets_path = "/Users/ivanoschepkov/Development/Empatika/coffee-automatization-ios/DoubleB/Targets"
new_plist = "/Users/ivanoschepkov/Desktop/AppDeployer/AppDeployer/Scripts/newApp/templateApp/templateApp-info.plist"

for d in os.listdir(targets_path):
	if os.path.isdir(os.path.join(targets_path, d)) and d != 'TestApp':
		f = "%s/%s/%s-info.plist" % (targets_path, d, d)
		pl = plistlib.readPlist(f)

		bundle_id = pl['CFBundleIdentifier']
		bundle_name = pl['CFBundleDisplayName']

		bundle_version = pl['CFBundleShortVersionString']
		bundle_build = pl['CFBundleVersion']


		print d
		m = re.search('.+\.(.+)', bundle_id)
		bundle_id_short = m.group(1)

		# print bundle_id
		# print bundle_name
		# print bundle_version
		# print bundle_id
		# print bundle_id_short

		os.remove(f)
		shutil.copyfile(new_plist, f)

		pl = plistlib.readPlist(f)
		pl['CFBundleIdentifier'] = bundle_id
		pl['CFBundleDisplayName'] = bundle_name
		pl['CFBundleShortVersionString'] = bundle_version
		pl['CFBundleVersion'] = bundle_build

		pl['FacebookUrlSchemeSuffix'] = bundle_id_short
		fb_scheme = pl['CFBundleURLTypes'][0]['CFBundleURLSchemes'][0]
		pl['CFBundleURLTypes'][0]['CFBundleURLSchemes'][0] = "%s%s" % (fb_scheme, bundle_id_short)

		plistlib.writePlist(pl, f)
require 'xcodeproj'

project_path = '/Users/ivanoschepkov/Desktop/TemplateApps/TemplateApps.xcodeproj'
target_name = 'EmpatikaApp'
profile_name = 'EmpatikaHouseCafe-AppStore'

project = Xcodeproj::Project.open(project_path)
project.targets.each do |target|
	if target.name == target_name
		target.build_configuration_list.build_configurations.each do |config|
			if config.name == 'Release'
				config_hash = config.build_settings
				config_hash["PROVISIONING_PROFILE_SPECIFIER"] = profile_name
			end
		end
	end
end

project.save()
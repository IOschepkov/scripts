import os
import glob
import sys
import shutil
import subprocess
import plistlib
import json
import re

targets_path = "/Users/ivanoschepkov/Development/Empatika/coffee-automatization-ios/DoubleB/Targets"

for d in os.listdir(targets_path):
	if os.path.isdir(os.path.join(targets_path, d)):
		print d
		f = "%s/%s/Info.plist" % (targets_path, d)
		pl = plistlib.readPlist(f)

		pl['NSCameraUsageDescription'] = 'We use your camera to scan barcodes'
		pl['NSPhotoLibraryUsageDescription'] = 'We use photo library to store your photos'

		plistlib.writePlist(pl, f)
import os
import glob
import sys
import shutil
import subprocess
import plistlib
import json

new_plist_path = sys.argv[1]

base_url_namespace = 'aaaa'

# Modify plist
pl = plistlib.readPlist(new_plist_path)

url_schemes = pl['CFBundleURLTypes'][0]['CFBundleURLSchemes']
new_url_schemes = []
for url_scheme in url_schemes:
	new_url_scheme = url_scheme
	if url_scheme.startswith('fb'):
		new_url_scheme = '%s%s' % (url_scheme, base_url_namespace)
	new_url_schemes.append(new_url_scheme)

pl['CFBundleURLTypes'][0]['CFBundleURLSchemes'] = new_url_schemes

pl['FacebookUrlSchemeSuffix'] = base_url_namespace

plistlib.writePlist(pl, new_plist_path)